FROM debian:10

MAINTAINER Tchit10 "tchit10@outlook.com"

RUN apt-get update && apt-get install -y \
    python3-pip \
 && rm -rf /var/lib/apt/lists/*

COPY ./requirements.txt /

RUN pip3 install -r requirements.txt

COPY flask /flask

WORKDIR /flask/flask_blog

RUN python3 init_db.py

EXPOSE 5000

ENTRYPOINT [ "python3" ]

CMD [ "app.py" ]
